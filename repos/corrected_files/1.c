#include<stdio.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include"pipe1.c"
#include"pipea1.c"
#include"pipeb1.c"
#include"pipec1.c"
#include"pipe4.c"


int main(int argc,char *argv[])
{
   float A,Q,v,D,H,ad,a,tebs,ls,Ds,ss,sp,sf,w1,p,Ce,d;
   int typ,iccs,isp,dl,ds,np,w;
   float Cs;
   
   Q = atof(argv[1]);
   printf("Entered value of discharge(in metric cube per second): ");
   printf("%f\n",Q);
   
   v=atof(argv[2]);
   printf("Entered value of velocity of water through pipes(in metres per second): ");
   printf("%f\n",v);
   A=Q/v;
   printf("A=%f\n",A);
   
   printf("\nSelect the standard pipe internal diameter:");
   printf("\n Diameters available are given below:(in meters)\n");
   printf("________________________________________________\n");
   printf("0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2\n");
   printf("________________________________________________\n");
   d = atof(argv[3]);
   w = d*1000.0;
   printf("The internal diameter selected is: \n");
   printf("%f", d);
  if( w == 500 || w == 600 || w == 700 || w == 800 || w == 900 || w == 1000 || w == 1100 || w == 1200)
{  
   
   np=pipea1(A,d);
   printf("\nNumber of pipes= %d\n",np);

}
   
else
{
   printf("Selected diameter is not standard one\n");
   exit(0);
}
   
  
   printf("\nEntered type of loading for the culvert\n");
   printf("Enter 1 for Class AA tracked and 2 for Class A: ");
    typ=atoi(argv[4]);
   printf("%d\n",typ);
   
   if(typ!=1&&typ!=2)
   {
      printf("\nSorry,it is absurd");
   }
   if(typ==1){ p=700;}
   if(typ==2){ p=228;}
   H=atof(argv[5]);
   printf("Entered height of the embankment in metres: ");
   printf("%f\n",H);
   Cs=pipe1(H,d);
   printf("Cs= %f", Cs);
   printf("\n The value of influence coefficient is %f",Cs);
   Ce=atof(argv[6]);
   printf("\n Entered value of coefficient of earth pressure from the code: ");
   printf("%f\n",Ce);
   
   if(w==500) {D=0.65;ls=2.78;ss=6.82;tebs=35.93;printf("Outer diameter= %f",D);}
   if(w==600) {D=0.77;ls=3.18;ss=9.01;tebs=43.11;printf("Outer diameter= %f",D);}
   if(w==700) {D=0.87;ls=3.18;ss=12.27;tebs=50.30;printf("Outer diameter= %f",D);}
   if(w==800) {D=0.99;ls=4.66;ss=15.04;tebs=57.48;printf("Outer diameter= %f",D);}
   if(w==900) {D=1.10;ls=4.66;ss=20.30;tebs=64.67;printf("Outer diameter= %f",D);}
   if(w==1000) {D=1.23;ls=4.66;ss=23.52;tebs=71.85;printf("Outer diameter= %f",D);}
   if(w==1100) {D=1.33;ls=4.66;ss=29.99;tebs=79.00;printf("Outer diameter= %f",D);}
   if(w==1200) {D=1.44;ls=5.55;ss=35.57;tebs=86.22;printf("Outer diameter= %f",D);}
   w1=atof(argv[7]);
   
   printf("\nEntered density of soil(in kN/m*m*m): ");
   printf("%f\n",w1);
   
   sf=pipe4(Ce,w1,Cs,p,tebs);
   printf("Factor of safety is= ",sf=(((tebs/1.5)-((6*Cs*p)/1.5)))/(Ce*w1*D*D));
   if(sf<=1.0)
      printf("Earthen bedding is provided\n");
   else
      if(sf>1.0&&sf<=2.7)
         printf("First class bedding is provided\n");
   Ds=7850;
   ad=(d+D)*0.5;
   printf("Average diameter= \n %f",ad);
   
   printf("\nChosen diameter of mild steel to be used as spiral reinforcement: ");

   ds=atoi(argv[8]);
   printf("%d\n",ds);

  
   iccs=pipeb1(ss,ad,Ds,ds);
   printf("\nC/C spacing of %d mm spirals is %d mm\n", ds, iccs);

   printf("\nChosen diameter of mild steel to be used as longitudinal reinforcement: ");

   dl=atoi(argv[9]);
   printf("%d\n",dl);
  
   
   isp=pipec1(ls,ad,Ds,dl);
   printf("\nC/C spacing of %d mm longitudinal steel is %d mm\n" ,dl, isp);
}
