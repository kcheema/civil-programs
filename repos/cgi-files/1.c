#include<stdio.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include"pipe1.c"
#include"pipea1.c"
#include"pipeb1.c"
#include"pipec1.c"
#include"pipe4.c"

int main(int argc,char *argv[])
{
   float A,Q,v,D,H,ad,a,tebs,ls,Ds,ss;
   float sp,sf,w1,p,Ce,d;
   int typ,np,iccs,isp,dl,ds;
   double Cs;
   
   Q = atof(argv[1]);
   printf("Entered value of discharge(in metric cube per second): ");
   
   printf("%f\n",Q);
   
   v=(atof(argv[2]));
   printf("Entered value of velocity of water through pipes(in metres per second): ");
   
   printf("%f\n",v);
   A=Q/v;
   d=atof(argv[3]);
   printf("A=%f\n",A=Q/v);
   printf("\nSelect the standard pipe internal diameter:");
   printf("\n Diameters available are given below:(in meters)\n");
   printf("________________________________________________\n");
   printf("0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2\n");
   printf("________________________________________________\n");
   
   printf("The internal diameter selected is: ");
   printf("%f\n",d);
   
   np=pipea1(A,d);
   printf("\nNumber of pipes= %d\n",np);
   
   typ=(atoi(argv[4]));
   printf("\nEntered the type of loading for the culvert\n");
   printf("Enter 1 for Class AA tracked and 2 for Class A: ");
   printf("%d\n",typ);
   
   if(typ!=1&&typ!=2)
   {
      printf("\nSorry,it is absurd");
   }
   if(typ==1) p=700;
   if(typ==2) p=228;
   H=atof(argv[5]);
   printf("Entered the height of the embankment in metres: ");
   printf("%f\n",H);
   
   Cs=pipe1(H,d);
   printf("\n The value of influence coefficient for embankment height %f",H);
   printf("and diameter %f is= %f \n", d , Cs );
   Ce=atof(argv[6]);
   printf("\nEntered the value of coefficient of earth pressure from the code: ");
   printf("%f\n",Ce);
   
   if(d==0.5) {D=0.65;ls=2.78;ss=6.82;tebs=35.93;};
   if(d==0.6) {D=0.77;ls=3.18;ss=9.01;tebs=43.11;};
   if(d==0.7) {D=0.87;ls=3.18;ss=12.27;tebs=50.30;};
   if(d==0.8) {D=0.99;ls=4.66;ss=15.04;tebs=57.48;};
   if(d==0.9) {D=1.10;ls=4.66;ss=20.30;tebs=64.67;};
   if(d==1.0) {D=1.23;ls=4.66;ss=23.52;tebs=71.85;};
   if(d==1.1) {D=1.33;ls=4.66;ss=29.99;tebs=79.00;};
   if(d==1.2) {D=1.44;ls=5.55;ss=35.57;tebs=86.22;};
   w1=atof(argv[7]);
   printf("Outer diameter\n",D);
   printf("Entered the density of soil(in kN/m*m*m): ");
   printf("%f\n",w1);
   
   sf=pipe4(Ce,w1,Cs,p,tebs);
   printf("Factor of safety is= ",sf=(((tebs/1.5)-((6*Cs*p)/1.5))/(Ce*w1*D*D)));
   if(sf<=1.0)
      printf("Earthen bedding is provided\n");
   else
      if(sf>1.0&&sf<=2.7)
         printf("First class bedding is provided\n");
   Ds=7850;
   ad=(d+D)*0.5;
   printf("Average diameter\n",ad);
   
   printf("\nFrom range 8 mm to 12 mm,\nEntered the diameter of mild steel to be used as spiral reinforcement: ");

   ds=atoi(argv[8]);
   printf("%d\n",ds);

   if(ds<8||ds>12)
  {
   printf("\n<h1>You have entered a value out of range</h1><br>");
   printf("<a href='http://localhost/pipe.html'> <u>GO BACK</u> </a>");
   exit(0);
  }  
  
   iccs=pipeb1(ss,ad,Ds,ds);
   printf("\nC/C spacing of %d mm spirals is %d mm\n", ds, iccs);

   printf("\nFrom range 6 mm to 8 mm,\nEntered diameter of mild steel to be used as longitudinal reinforcement: ");

   dl=atoi(argv[9]);
   printf("%d\n",dl);
  
   if(dl<6||dl>8)
   {
    printf("\n <h1> You have entered a value out of range</h1><br>");
    printf("<a href='http://localhost/pipe.html'> <u>GO BACK</u></a>");
    exit(0);
   }

   isp=pipec1(ls,ad,Ds,dl);
   printf("\nC/C spacing of %d mm longitudinal steel is %d mm\n" ,dl, isp);
}

