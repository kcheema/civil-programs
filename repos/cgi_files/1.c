#include<stdio.h>
#include<math.h>
#include"pipe1.c"
#include"pipea1.c"
#include"pipeb1.c"
#include"pipec1.c"
#include"pipe4.c"
int main(int argc,char *argv[])
{
float A,Q,v,D,H,ad,a,tebs,ls,Ds,ss;
float sp,sf,w1,p,Ce,d;
int typ,np,iccs,isp,dl,ds;
double Cs;
Q=(atof(argv[1]));
printf("Enter the value of discharge(in metric cube per second):");
//scanf("%f",&Q);
v=(atof(argv[2]));
printf("Enter the value of velocity of water through pipes(in metres per second):");
//scanf("%f",&v);
//A=Q/v;
A=(atof(argv[3]));
printf("A=%f/n",A);
printf("\n Select the standard pipe internal diameter:\n");
printf("\n Diameters available are given below:(in meters)");
printf("\n________________________________________________");
printf("\n 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2");
printf("\n________________________________________________");
do
{
d=(atof(argv[4]));
printf("\n Enter the internal diameter selected:");
//scanf("%f",&d);
if((d!=0.5)&&(d!=0.6)&&(d!=0.7)&&(d!=0.8)&&(d!=0.9)&&(d!=1.0)&&(d!=1.1)&&(d!=1.2))
{
printf("\nSelected diameter is not standard one");
}}
while(d!=0.5&&d!=0.7&&d!=0.8&&d!=0.9&&d!=1.0&&d!=1.1&&d!=1.2);
np=pipea1(A,d);
printf("\n Number of pipes: %d",np);
do
{
typ=(atoi(argv[5]));
printf("\n Enter the type of loading for the culvert");
printf("\n Enter 1 for Class AA tracked and 2 for Class A");
//scanf("%d",&typ);
if(typ!=1&&typ!=2)
{
printf("\nSorry,it is absurd");
}}
while(typ!=1&&typ!=2);
if(typ==1) p=700;
if(typ==2) p=228;
H=(atof(argv[6]));
printf("\n Enter the height of the embankment in metres");
//scanf("%f",&H);
Cs=pipe1(H,d);
printf("\n The value of influence coefficient for embankment height %f ",H);
printf("and diameter %f is: %f \n", d , Cs );
Ce=(atof(argv[7]));
printf("\n Enter the value of coefficient of earth pressure from the code");
//scanf("%f",&Ce);
if(d==0.5) {D=0.65;ls=2.78;ss=6.82;tebs=35.93;};
if(d==0.6) {D=0.77;ls=3.18;ss=9.01;tebs=43.11;};
if(d==0.7) {D=0.87;ls=3.18;ss=12.27;tebs=50.30;};
if(d==0.8) {D=0.99;ls=4.66;ss=15.04;tebs=57.48;};
if(d==0.9) {D=1.10;ls=4.66;ss=20.30;tebs=64.67;};
if(d==1.0) {D=1.23;ls=4.66;ss=23.52;tebs=71.85;};
if(d==1.1) {D=1.33;ls=4.66;ss=29.99;tebs=79.00;};
if(d==1.2) {D=1.44;ls=5.55;ss=35.57;tebs=86.22;};
w1=(atof(argv[8]));
printf("Outer diameter\n",D);
printf("\n Enter the density of soil(in kN/m*m*m):");
//scanf("%f",&w1);
sf=pipe4(Ce,w1,Cs,p,tebs);
printf("Factor of safety is \n",sf);
if(sf<=1.0)
printf("\n Earthen bedding is provided");
else
if(sf>1.0&&sf<=2.7)
printf("\n First class bedding is provided\n");
Ds=7850;
ad=(d+D)*0.5;
printf("\n Average diameter\n",ad);
ds=(atoi(argv[9]));
ds=pipeb();
iccs=pipeb1(ss,ad,Ds,ds);
printf("\n C/C spacing of %d mm spirals is %d mm.\n", ds, iccs);
dl=(atoi(argv[10]));
dl=pipec();
isp=pipec1(ls,ad,Ds,dl);
printf("\n C/C spacing of %d mm longitudinal steel is %d mm\n" ,dl, isp);
//getch();
}
